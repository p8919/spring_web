package hrd.crud_home_word;

import hrd.crud_home_word.repositioy.ContentRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudHomeWordApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudHomeWordApplication.class, args);
//        ContentRepository contentRepository = new ContentRepository();
//        contentRepository.getAllLanguages();
    }

}
